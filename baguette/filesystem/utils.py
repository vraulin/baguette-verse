"""
This module defines some useful objects for building the Baguette file system.
(All of this because Python devs decided to deprecate TracebackException.exc_type...What a good idea!)
"""

from traceback import TracebackException
from types import TracebackType

__all__ = ["InspectTracebackException"]





class InspectTracebackException(TracebackException):

    """
    A subclass of TracebackException that has an exc_type field! Just like before Python 3.13!
    """

    def __init__(self, exc_type: type[BaseException], exc_value: BaseException, exc_traceback: TracebackType | None, *, limit: int | None = None, lookup_lines: bool = True, capture_locals: bool = False, compact: bool = False, max_group_width: int = 15, max_group_depth: int = 10, save_exc_type: bool = True, _seen: set[int] | None = None) -> None:
        self.__exc_type = exc_type
        super().__init__(exc_type, exc_value, exc_traceback, limit=limit, lookup_lines=lookup_lines, capture_locals=capture_locals, compact=compact, max_group_width=max_group_width, max_group_depth=max_group_depth, save_exc_type=save_exc_type, _seen=_seen)

    @property
    def exc_type(self) -> type[BaseException]:
        """
        The class of the exception.
        """
        return self.__exc_type
    




del TracebackException, TracebackType