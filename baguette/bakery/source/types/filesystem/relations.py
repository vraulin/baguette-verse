"""
This module contains Edge and Arrow subclasses for this behavioral package.
"""

from typing import Literal
from .....logger import logger
from ...graph import Arrow, DataEdge, Edge
from ..data.entities import Data
from ..execution.entities import Call, Process
from ..network.entities import Host
from .entities import Directory, File, FileHandle

__all__ = ["UsesFile", "UsesDirectory", "OpensFile", "CreatesFile", "CreatesDirectory", "DeletesFile", "DeletesDirectory", "Reads", "Writes", "Conveys", "HasDrive", "Contains", "IsCopyiedInto"]





logger.info("Loading relations from {} library.".format(__name__.rpartition(".")[0].rpartition(".")[2]))

class UsesFile(Edge[FileHandle, File]):

    """
    This kind of edge indicates that a handle uses a file.
    """

    label : str = ""





class UsesDirectory(Edge[FileHandle, Directory]):

    """
    This kind of edge indicates that a handle uses a directory.
    """

    label : str = ""





class OpensFile(Edge[Call, FileHandle]):

    """
    This kind of edge indicates that a system call opened a handle.
    """

    label : str = ""





class CreatesFile(Edge[Call, FileHandle]):

    """
    This kind of edge indicates that a system call created a file through a handle.
    """

    label : str = ""





class CreatesDirectory(Edge[Call, FileHandle]):

    """
    This kind of edge indicates that a system call created a directory through a handle.
    """

    label : str = ""





class DeletesFile(Edge[Call, FileHandle]):

    """
    This kind of edge indicates that a system call deleted a file through a handle.
    """

    label : str = ""





class DeletesDirectory(Edge[Call, FileHandle]):

    """
    This kind of edge indicates that a system call deleted a directory through a handle.
    """

    label : str = ""





class Reads(Edge[Call, Data]):

    """
    This kind of arrow indicates that a system call read data from a file.
    """





class Writes(Edge[Call, Data]):

    """
    This kind of arror indicates that a system call wrote data to a file.
    """





class Conveys(DataEdge[Data, FileHandle]):

    """
    This kind of arrow indicates a data flux with a file handle.
    """

    label : str = ""

    __slots__ = {
        "__mode" : "The mode of the data flux (read or write)"
    }

    __stateful_data__ = {
        "mode"
    }

    __str_data__ = {
        "mode"
    }

    def __init__(self, source : Data, destination : FileHandle, *, auto_write : bool = True, mode : Literal["read", "write"], **stateful_data):
        super().__init__(source=source, destination=destination, auto_write=auto_write, mode = mode, **stateful_data)

    @property
    def mode(self) -> Literal["read", "write"]:
        """
        The mode of the data flux. (read or write)
        """
        return self.__mode
    
    @mode.setter
    def mode(self, value : Literal["read", "write"]):
        if value not in ["read", "write"]:
            raise ValueError("Invalid mode value.")
        self.__mode : 'Literal["read", "write"]' = value





class HasDrive(Edge[Host, Directory | File]):

    """
    This kind of edge indicates that a machine owns a disk drive/partition(/root directory).
    """

    label : str = ""





class Contains(Arrow[Directory, Directory | File]):

    """
    This kind of arrow indicates that a directory contains a file or another directory.
    """

    label : str = ""





class IsCopyiedInto(Arrow[File, File]):

    """
    This kind indicates that the destination file is a copy of the source file.
    """





del Arrow, Call, Data, Directory, Edge, DataEdge, File, FileHandle, Host, Process, Literal, logger