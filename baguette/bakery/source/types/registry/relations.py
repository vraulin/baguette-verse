"""
This module contains Edge and Arrow subclasses for this behavioral package.
"""

from .....logger import logger
from ...graph import Arrow, Edge
from ..execution.entities import Process, Call
from ..filesystem.entities import Directory, File
from ..network.entities import Host
from .entities import KeyHandle, Key, KeyEntry

__all__ = ["HasSubKey", "HasEntry", "UsesKey", "OpensKey", "Discovered", "Enumerates", "QueriesEntry", "ReadsEntry", "SetsEntry", "WritesEntry", "DeletesEntry", "ErasesEntry", "ChangesTowards", "ReferencesFileSystem"]





logger.info("Loading relations from {} library.".format(__name__.rpartition(".")[0].rpartition(".")[2]))

class HasSubKey(Arrow[Host | Key, Key]):

    """
    This kind of arrow indicates that a key has a sub-key.
    """

    label : str = ""





class HasEntry(Edge[Key, KeyEntry]):

    """
    This kind of edge indicates that a registry key has one registry entry.
    """

    label : str = ""





class UsesKey(Edge[KeyHandle, Key]):

    """
    This kind of edge indicates that a handle uses a registry key.
    """

    label : str = ""





class OpensKey(Edge[Call, KeyHandle]):

    """
    This kind of edge indicates that an API call opened a registry key.
    """





class Discovered(Edge[KeyHandle, Key]):

    """
    This kind of edge indicates that a key handle discovered a sub-key throught enumeration.
    """





class Enumerates(Edge[Call, KeyHandle]):

    """
    This kind of edge indicates that an API call enumerates registry keys.
    """





class QueriesEntry(Edge[KeyEntry, KeyHandle]):

    """
    This kind of edge indicates that a key entry was queried by a key handle. 
    """





class ReadsEntry(Edge[Call, KeyHandle]):

    """
    This kind of edge indicates that an API call read a registry key entry.
    """





class SetsEntry(Edge[KeyHandle, KeyEntry]):

    """
    This kind of edge indicates that a key handle set the value of key entry.
    """





class WritesEntry(Edge[Call, KeyHandle]):

    """
    This kind of edge indicates that an API call wrote a registry key entry.
    """





class DeletesEntry(Edge[KeyHandle, KeyEntry]):

    """
    This kind of edge indicates that a key handle deleted a key entry.
    """





class ErasesEntry(Edge[Call, KeyHandle]):
    
    """
    This kind of edge indicates that an API call erased a registry key entry.
    """





class ChangesTowards(Arrow[KeyEntry, KeyEntry]):

    """
    This kind of arrow indicates that a key entry's content changed to another.
    """

    label : str = ""





class ReferencesFileSystem(Edge[KeyEntry, File | Directory]):
    
    """
    This kind of edge indicates that a key's entry references a file or a folder.
    """





del Arrow, Directory, Edge, File, KeyHandle, Host, Key, KeyEntry, Process, logger