"""
This module contains Edge and Arrow subclasses for this behavioral package.
"""

from .....logger import logger
from ...config import SwitchSetting
from ...colors import Color
from ...graph import Arrow, DataEdge, Edge, Vertex
from ..data.entities import Data
from ..execution.entities import Call, Process
from .entities import Connection, Host, Socket

__all__ = ["SpawnedProcess", "HasSocket", "HasConnection", "Communicates", "CreatesSocket", "Binds", "Connects", "Sends", "Receives", "Conveys", "Closes", "ListensOn", "Shutsdown", "Accepts", "Duplicates"]





logger.info("Loading relations from {} library.".format(__name__.rpartition(".")[0].rpartition(".")[2]))

class SpawnedProcess(Edge[Host, Process]):
    
    """
    This kind of edge indicates that a machine hosts a process.
    """





class HasSocket(Edge[Process, Socket]):

    """
    This kind of edge indicates that a process opened a socket.
    """

    label : str = ""





class HasConnection(Edge[Socket, Connection]):

    """
    This kind of edge indicates that a socket makes a connection.
    """

    label : str = ""





class Communicates(Edge[Connection, Host]):

    """
    This kind of edge indicates that two hosts communicate via a connection
    """





class CreatesSocket(Edge[Call, Socket]):

    """
    This kind of edge indicates that a system call created a socket.
    """

    label : str = ""





class Binds(DataEdge[Call, Connection]):

    """
    This kind of edge indicates that a system call bound a connection to a local address.
    """

    __slots__ = {
        "__src" : "The source (local) address of the connection",
    }

    __defining_data__ = {
        "src"
    }

    __stateful_data__ = {
        "src"
    }

    __str_data__ = {
        "src"
    }

    label : str = ""

    def __init__(self, source: Call, destination: Connection, *, auto_write: bool = True, src : tuple[str, int], **stateful_data) -> None:
        super().__init__(source, destination, auto_write = auto_write, src = src, **stateful_data)

    @property
    def src(self) -> tuple[str, int]:
        """
        The local address of the connection.
        """
        return self.__src
    
    @src.setter
    def src(self, s : tuple[str, int]):
        if not isinstance(s, tuple):
            raise TypeError(f"Expected tuple, got '{type(s).__name__}'")
        if len(s) != 2:
            raise ValueError(f"Expected a length 2 tuple, got {len(s)}")
        if not isinstance(s[0], str) or not isinstance(s[1], int):
            raise TypeError(f"Expected tuple of str and int, got '{type(s[0]).__name__}' and '{type(s[1]).__name__}'")
        self.__src = s





class Connects(DataEdge[Call, Connection]):

    """
    This kind of edge indicates that a system call connected a connection to a remote address.
    """

    __slots__ = {
        "__dst" : "The destination (remote) address of the connection"
    }

    __defining_data__ = {
        "dst"
    }

    __stateful_data__ = {
        "dst"
    }

    __str_data__ = {
        "dst"
    }

    label : str = ""

    def __init__(self, source: Call, destination: Connection, *, auto_write: bool = True, dst : tuple[str, int], **stateful_data) -> None:
        super().__init__(source, destination, auto_write = auto_write, dst = dst, **stateful_data)

    @property
    def dst(self) -> tuple[str, int]:
        """
        The remote address of the connection.
        """
        return self.__dst
    
    @dst.setter
    def dst(self, d : tuple[str, int]):
        if not isinstance(d, tuple):
            raise TypeError(f"Expected tuple, got '{type(d).__name__}'")
        if len(d) != 2:
            raise ValueError(f"Expected a length 2 tuple, got {len(d)}")
        if not isinstance(d[0], str) or not isinstance(d[1], int):
            raise TypeError(f"Expected tuple of str and int, got '{type(d[0]).__name__}' and '{type(d[1]).__name__}'")
        self.__dst = d





class Sends(Edge[Call, Data]):

    """
    This kind of arrow indicates that a system call sent data through a connection.
    """





class Receives(Edge[Call, Data]):

    """
    This kind of arrow indicates that a system call received data through a connection.
    """





class Conveys(Edge[Connection, Data]):
    
    """
    This kind of arrow indicates that a connection conveyed a message.
    """

    label : str = ""





class Closes(Edge[Call, Connection]):

    """
    This kind of edge indicates that a system call closed a connection.
    """
    
    label : str = ""





class ListensOn(Edge[Call, Socket]):

    """
    This kind of edge indicates that a system call set a socket to listening mode.
    """





class Shutsdown(Edge[Call, Connection]):

    """
    This kind of edge indicates that a connection was shutdown by a system call.
    """

    label : str = ""





class Accepts(DataEdge[Call, Connection]):

    """
    This kind of arrow indicates that a system call accepted a connection from a remote address.
    """

    __slots__ = {
        "__dst" : "The destination (remote) address of the connection"
    }

    __defining_data__ = {
        "dst"
    }

    __stateful_data__ = {
        "dst"
    }

    __str_data__ = {
        "dst"
    }

    label : str = ""

    def __init__(self, source: Call, destination: Connection, *, auto_write: bool = True, dst : tuple[str, int], **stateful_data) -> None:
        super().__init__(source, destination, auto_write = auto_write, dst = dst, **stateful_data)

    @property
    def dst(self) -> tuple[str, int]:
        """
        The remote address of the connection.
        """
        return self.__dst
    
    @dst.setter
    def dst(self, d : tuple[str, int]):
        if not isinstance(d, tuple):
            raise TypeError(f"Expected tuple, got '{type(d).__name__}'")
        if len(d) != 2:
            raise ValueError(f"Expected a length 2 tuple, got {len(d)}")
        if not isinstance(d[0], str) or not isinstance(d[1], int):
            raise TypeError(f"Expected tuple of str and int, got '{type(d[0]).__name__}' and '{type(d[1]).__name__}'")
        self.__dst = d





class Duplicates(Arrow[Socket, Socket]):

    """
    This kind of arrow indicates that a socket was duplicated to form a similar socket (example: after a call to accept()).
    """





del Arrow, DataEdge, Call, Color, SwitchSetting, Connection, Data, Edge, Host, Process, Socket, Vertex, logger