"""
This module contains Edge and Arrow subclasses for this behavioral package.
"""

from .....logger import logger
from ...config import SwitchSetting
from ...colors import Color
from ...graph import Arrow, DataEdge, Edge, Vertex
from ..filesystem.entities import Directory, File
from .entities import Call, Handle, Process, Thread

__all__ = ["HasChildProcess", "UsesAsArgument", "Runs", "HasThread", "HasFirstCall", "FollowedBy", "NextSignificantCall", "StartedThread", "InjectedThread", "StartedProcess", "Closes", "HasHandle"]





logger.info("Loading relations from {} library.".format(__name__.rpartition(".")[0].rpartition(".")[2]))

class HasChildProcess(Arrow[Process, Process]):

    """
    This kind of arrow indicates that a process created another one.
    """





class UsesAsArgument(Edge[Process, File | Directory]):

    """
    This kind of edge indicates that a process used a file or directory in its command line.
    """




class Runs(Edge[Process, File]):
    
    """
    This kind of edge indicates process ran a file as its program.
    """





class HasThread(Edge[Process, Thread]):

    """
    This kind of edge indicates that a process hosts a thread.
    """

    label : str = ""





class HasFirstCall(Edge[Thread, Call]):

    """
    This kind of edge indicates what was the first system call of a thread.
    """

    label : str = ""





class FollowedBy(Arrow[Call, Call]):

    """
    This kind of arrow indicates that a system call was followed by another one.
    """

    label : str = ""





class NextSignificantCall(Arrow[Call, Call]):
    
    """
    This kind of arrows links two Call nodes that happened one after the other in the same thread both nodes are linked to other types of nodes.
    """

    label : str = ""





class StartedThread(DataEdge[Call, Thread]):

    """
    This kind of edge indicates that a system call started a new thread.
    """

    __slots__ = {
        "__remote" : "Indicates if the started thread was started in another process (remote thread)"
    }

    __defining_data__ = {
        "remote"
    }

    __stateful_data__ = {
        "remote"
    }

    __str_data__ = {
        "remote"
    }

    label : str = ""

    def __init__(self, source: Call, destination: Thread, *, auto_write: bool = True, remote : bool, **stateful_data) -> None:
        super().__init__(source, destination, auto_write = auto_write, remote = remote, **stateful_data)

    @property
    def remote(self) -> bool:
        """
        Indicates if this the started Thread belongs to a different Process than the one that started it.
        """
        return self.__remote
    
    @remote.setter
    def remote(self, r : bool):
        if not isinstance(r, bool):
            raise TypeError(f"Expected bool, got '{type(r).__name__}'")
        self.__remote = r
    




class InjectedThread(Arrow[Process, Thread]):

    """
    This kind of arrow indicates a process created a remote thread.
    """





class StartedProcess(Edge[Call, Process]):

    """
    This kind of edge indicates that a system call started a new process.
    """

    label : str = ""





class Closes(Edge[Call, Handle]):

    """
    This kind of edge indicates that a system call closed a handle.
    """

    label : str = ""





class HasHandle(Edge[Process, Handle]):

    """
    This kind of edge indicates that a process owns a handle.
    """

    label : str = ""





del Arrow, DataEdge, Call, Color, SwitchSetting, Directory, Edge, File, Process, Thread, Handle, Vertex, logger