from subprocess import run, DEVNULL, PIPE
from sys import executable
from tempfile import TemporaryDirectory
from os import cpu_count
from pathlib import Path
from argparse import ArgumentParser
from threading import Thread
from glob import glob
from alive_progress import alive_bar

NCPU = cpu_count() or 1

unknown_baking_error_exit_code = run([executable, "-m", "baguette.exit_codes", "BAKING_UNKNOWN_EXCEPTION"], stdout=DEVNULL).returncode





parser = ArgumentParser()

parser.add_argument("output", type=Path, help="The failed compilation output folder")
parser.add_argument("inputs", type=Path, nargs="+", help="The input CAPE files")

args = parser.parse_args()

output : Path = args.output
pre_inputs : list[Path] = args.inputs

if output.exists() and not output.is_dir():
    parser.error(f"output file exists and is not a directory: '{output}'")
output.mkdir(parents=True, exist_ok=True)
inputs : list[Path] = []
for p in pre_inputs:
    inputs.extend(Path(pi) for pi in glob(str(p)))
for p in inputs:
    if not p.is_file():
        parser.error(f"input file does not exist or is not a file: '{p}'")

print(f"Got {len(inputs)} jobs.")

def handle_job(p : Path):
    with TemporaryDirectory() as working_dir:
        cmd = [
            executable,
            "-m",
            "baguette.bake",
            "--raw",
            f"{p}",
            working_dir,
            "--filters",
            "injected_threads_only",
            "modified_registry_only",
            "no_data_nodes",
            "no_handle_nodes",
            "no_simple_imports",
            "significant_processes_only",
            "significant_call_only",
            ]
        proc = run(cmd, encoding="utf-8", stdout=DEVNULL, stderr=PIPE)
        ret = proc.returncode
        # print(f"{" ".join(cmd)} -> {proc.returncode}")
        if ret & unknown_baking_error_exit_code:
            print(f"Found a failing Cape compilation: '{p.name}'")
            with open(f"{output}/{p.stem}.log", "w") as f:
                f.write(proc.stderr)
        bar()


def handle_jobs():
    while inputs:
        job = inputs.pop()
        handle_job(job)

with alive_bar(len(inputs), title="Baking CAPE reports") as bar:
    threads = [Thread(target=handle_jobs, name=f"Worker #{i}", daemon=True) for i in range(NCPU)]
    for t in threads:
        t.start()
    for t in threads:
        t.join()