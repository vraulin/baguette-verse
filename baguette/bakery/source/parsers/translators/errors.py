errors = {
    ('NtDeleteKey', 'regkey') : 15,
    ('NtCreateKey', 'regkey') : 4420,
    ('NtEnumerateKey', 'buffer') : 3338,
    ('CreateProcessWithTokenW', 'filepath') : 2,
    ('CreateThread', 'thread_identifier') : 2,
    ('NtSetValueKey', 'regkey') : 149,
    ('NtEnumerateValueKey', 'regkey') : 3624,
    ('RegEnumValueA', 'value') : 74,
}