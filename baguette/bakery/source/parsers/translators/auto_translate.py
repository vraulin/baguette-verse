"""
This command line module helps you build the translation table from a given source format into the destination format. Use '--help' for more info.
"""





def main():

    from argparse import ArgumentParser
    from collections import Counter
    from pathlib import Path
    from io import BufferedReader
    from json import dump
    from typing import Iterator, Callable, Literal, TYPE_CHECKING
    from threading import Event, RLock, Thread
    if TYPE_CHECKING:
        from baguette.bakery.source.parsers.utils import Translator
    from Viper.interactive import InteractiveInterpreter

    parser = ArgumentParser("auto_translate", description="Helps you build a translation table from input files from the source format, input files from the destination format and saves it.")

    parser.add_argument("source_format_directory", type=Path, help="A folder containing the files with the format to translate from.")
    parser.add_argument("destination_format_directory", type=Path, help="A folder containing the files with the format to translate to.")
    parser.add_argument("output", type=Path, help="The output file (a JSON file) that will contain the translation table.")
    parser.add_argument("--code", "-c", help="Executes the given Python code and exits if given, instead of opening the interactive prompt. The code is executed in the same environment than that of the interpreter.")

    def cuckoo_reader(file : BufferedReader) -> Iterator[tuple[str, dict[str, str | int]]]:
        """
        Parses a Cuckoo report. Yields API call names with their set of argument names.
        """
        from json import load
        data = load(file)
        if "behavior" in data:
            for proc in data["behavior"]["processes"]:
                for call in proc["calls"]:
                    assert not (set(call["flags"]) - set(call["arguments"]))
                    yield call["api"], call["flags"] | call["arguments"]

    def cape_reader(file : BufferedReader) -> Iterator[tuple[str, dict[str, str]]]:
        """
        Parses a Cape report. Yields API call names with their set of argument names.
        """
        from json import load
        data = load(file)
        if "behavior" in data:
            for proc in data["behavior"]["processes"]:
                for call in proc["calls"]:
                    names : dict[str, str] = {}
                    for arg in call["arguments"]:
                        names[arg["name"]] = arg["value"]
                    yield call["api"], names

    args = parser.parse_args()

    source_format_folder : Path = args.source_format_directory
    destination_format_folder : Path = args.destination_format_directory
    output : Path = args.output

    if not source_format_folder.is_dir():
        parser.error(f"source format path is not a directory or does not exist: '{source_format_folder}'")
    if not destination_format_folder.is_dir():
        parser.error(f"destination format path is not a directory or does not exist: '{destination_format_folder}'")
    if output.exists() and not output.is_file():
        parser.error(f"output path exists and is not a file: '{output}'")

    source_format_dict : dict[str, set[str]] = {}
    source_type_dict : dict[str, dict[str, Counter[Literal["int", "hex_string", "int_string", "str", "other"]]]] = {}
    source_format_scanning_progress : float = 0.0
    source_scanning_event = Event()
    source_scanning_complete : bool = False
    source_scanning_progress : tuple[int, int] = (0, 1)

    destination_format_dict : dict[str, set[str]] = {}
    destination_type_dict : dict[str, dict[str, Counter[Literal["int", "hex_string", "int_string", "str", "other"]]]] = {}
    destination_format_scanning_progress : float = 0.0
    destination_scanning_event = Event()
    destination_scanning_complete : bool = False
    destination_scanning_progress : tuple[int, int] = (0, 1)

    scanned_event = Event()
    state_lock = RLock()
    scanning_tick_event = Event()

    def progress() -> tuple[float, float]:
        """
        Returns the progress of the scanning of the source and destination formats.
        """
        print(f"Progress:")
        print(f"Source: {round(source_format_scanning_progress * 100, 2)}%\t\tDestination: {round(destination_format_scanning_progress * 100, 2)}%")
        return source_format_scanning_progress, destination_format_scanning_progress
    
    def infer_type(arg : str | int) -> Literal["int", "hex_string", "int_string", "str"]:
        """
        Scans the argument of a call and returns its type.
        """
        if isinstance(arg, int):
            return "int"
        elif isinstance(arg, str):
            if arg.lower().startswith("0x") and all(c.lower() in "0123456789abcdef" for c in arg[2:]):
                return "hex_string"
            elif arg.isnumeric():
                return "int_string"
            return "str"
        else:
            raise TypeError(f"Got a weird argument: {repr(arg)}")
    
    def start_scanning_source_format(reader : Callable[[BufferedReader], Iterator[tuple[str, dict[str, str | int]]]]) -> dict[str, set[str]]:
        """
        Starts the scanning of the source format files.
        Directly returns a dictionary indexed by the names of the API calls with values being the set of argument names.
        The scan is performed in a background thread.
        """
        with state_lock:
            if source_scanning_event.is_set():
                raise RuntimeError("Scanning is already ongoing. Wait...")

            def background_source_scan():
                nonlocal source_format_scanning_progress, source_scanning_complete, source_scanning_progress
                source_format_dict.clear()
                source_format_scanning_progress = 0.0
                source_scanning_complete = False
                scanned_event.clear()
                source_scanning_event.set()
                try:
                    files = list(source_format_folder.iterdir())
                    source_scanning_progress = (0, n := len(files))
                    for i, p in enumerate(files):
                        with p.open("rb") as f:
                            for call_name, args in reader(f):
                                source_format_dict.setdefault(call_name, set()).update(args)
                                args_types = source_type_dict.setdefault(call_name, {})
                                for name, value in args.items():
                                    if name not in args_types:
                                        args_types[name] = Counter()
                                    if isinstance(value, (int, str)):
                                        args_types[name][infer_type(value)] += 1
                                    else:
                                        args_types[name]["other"] += 1
                        source_format_scanning_progress = (i + 1) / len(files)
                        source_scanning_progress = (i + 1, n)
                        scanning_tick_event.set()
                    with state_lock:
                        source_scanning_complete = True
                        if destination_scanning_complete:
                            scanned_event.set()
                        source_scanning_event.clear()
                except:
                    with state_lock:
                        source_scanning_event.clear()
                    raise

            Thread(target = background_source_scan, daemon = True, name = "Source Format Background Scanner").start()

            source_scanning_event.wait()
            return source_format_dict
        
    def start_scanning_destination_format(reader : Callable[[BufferedReader], Iterator[tuple[str, dict[str, str | int]]]]) -> dict[str, set[str]]:
        """
        Starts the scanning of the destination format files.
        Directly returns a dictionary indexed by the names of the API calls with values being the set of argument names.
        The scan is performed in a background thread.
        """
        with state_lock:
            if destination_scanning_event.is_set():
                raise RuntimeError("Scanning is already ongoing. Wait...")

            def background_destination_scan():
                nonlocal destination_format_scanning_progress, destination_scanning_complete, destination_scanning_progress
                destination_format_dict.clear()
                destination_format_scanning_progress = 0.0
                destination_scanning_complete = False
                scanned_event.clear()
                destination_scanning_event.set()
                try:
                    files = list(destination_format_folder.iterdir())
                    destination_scanning_progress = (0, n := len(files))
                    for i, p in enumerate(files):
                        with p.open("rb") as f:
                            for call_name, args in reader(f):
                                destination_format_dict.setdefault(call_name, set()).update(args)
                                args_types = destination_type_dict.setdefault(call_name, {})
                                for name, value in args.items():
                                    if name not in args_types:
                                        args_types[name] = Counter()
                                    if isinstance(value, (int, str)):
                                        args_types[name][infer_type(value)] += 1
                                    else:
                                        args_types[name]["other"] += 1
                        destination_format_scanning_progress = (i + 1) / len(files)
                        destination_scanning_progress = (i + 1, n)
                        scanning_tick_event.set()
                    with state_lock:
                        destination_scanning_complete = True
                        if source_scanning_complete:
                            scanned_event.set()
                        destination_scanning_event.clear()
                except:
                    with state_lock:
                        destination_scanning_event.clear()
                    raise


            Thread(target = background_destination_scan, daemon = True, name = "Destination Format Background Scanner").start()

            destination_scanning_event.wait()
            return destination_format_dict
    
    def wait_scans():
        """
        Waits for scans to complete.
        """
        with state_lock:
            if (not source_scanning_event.is_set() and source_scanning_progress[0] < source_scanning_progress[1]) or (not destination_scanning_event.is_set() and destination_scanning_progress[0] < destination_scanning_progress[1]):
                raise RuntimeError("Scans have not all been started yet")
        from alive_progress import alive_bar
        with alive_bar(source_scanning_progress[1] + destination_scanning_progress[1], title = "Scanning API calls") as bar:
            while source_scanning_progress[0] < source_scanning_progress[1] or destination_scanning_progress[0] < destination_scanning_progress[1]:
                scanning_tick_event.wait()
                scanning_tick_event.clear()
                while bar.current < source_scanning_progress[0] + destination_scanning_progress[0]:
                    bar()

    def levenshtein_assignement_translate(src_names : set[str], dst_names : set[str]) -> dict[str, str]:
        """
        Returns the translation table from the source names to the destination names that minimizes the relative string distance.
        """
        from Levenshtein import distance

        def levenshtein_distance(s1 : str, s2 : str, *, insert_cost : int = 1, delete_cost : int = 1, substitute_cost : int = 1) -> int:
            """
            Levenshtein distance between two strings.
            """
            return distance(s1, s2, weights=(insert_cost, delete_cost, substitute_cost))
        
        def relative_levenshtein_distance(s1 : str, s2 : str, *, insert_cost : int = 1, delete_cost : int = 1, substitute_cost : int = 1) -> float:
            """
            Relative Levenshtein distance between two strings (distance to max possible distance between strings of this size ratio).
            """
            a, b = max(len(s1), len(s2)), min(len(s1), len(s2))
            return levenshtein_distance(s1, s2, insert_cost=insert_cost, delete_cost=delete_cost, substitute_cost=substitute_cost) / max(b * delete_cost + a * insert_cost, b * substitute_cost + (a - b) * insert_cost)
        
        def case_translate(s : str) -> str:
            """
            Transforms a string from PascalCase to snake_case.
            """
            res : list[str] = []
            for c in s:
                if c.isupper() and res:
                    res.append("_")
                res.append(c.lower())
            return "".join(res)


        if not src_names:
            return {}
        
        try:
            from munkres import Munkres
        except ModuleNotFoundError as e:
            e.add_note("You need the munkres module to solve the assignement problem between API call argument names. Use 'pip install munkres==1.1.4'.")

        mnkrs = Munkres()
        base_src = tuple(src_names)
        base_dst = tuple(dst_names)
        matrix = [[levenshtein_distance(src.lower(), dst.lower()) for dst in base_dst] for src in base_src]
        best_idxs = mnkrs.compute(matrix) # type: ignore

        translation : dict[str, str] = {}
        for k, l in best_idxs:
            s1, s2 = base_src[k], base_dst[l]
            if relative_levenshtein_distance(s1, s2) > 0.5:
                translation[s1] = case_translate(s1)
            else:
                translation[s1] = s2

        for name in base_src:
            if name not in translation:
                translation[name] = case_translate(name)

        return translation


    def translate(*translation_heuristics : Callable[[set[str], set[str]], dict[str, str] | None]):
        """
        Performs the translation from the scanned files and with the given translation heuristics.
        A translation heuristic takes the set of argument names from the source format and destination format and should return the mapping of argument names from source to destination.
        It can also return None which will pass on to the next given heuristic.
        If no heuristic can translate, raises RuntimeError.
        If it can translate all calls, saves the translation table in the given output file and exists.
        """
        from alive_progress import alive_bar
        while True:
            wait_scans()
            state_lock.acquire()
            if not scanned_event.is_set():
                state_lock.release()
            else:
                break
        
        names_index : dict[str, str] = {}
        argument_names_table : dict[str, dict[str, str]] = {}
        argument_types_table : dict[str, dict[str, Translator.FUNC_NAMES]] = {}
        translation_table = {
            "Names" : names_index,
            "Argument Name Translations" : argument_names_table,
            "Argument Converters" : argument_types_table
        }

        def translate_call(call_name : str, source_args : set[str], destionation_args : set[str]):
            for translator in translation_heuristics:
                res = translator(source_args, destionation_args)
                if res is not None:
                    if not all(src_name in res for src_name in source_args):
                        raise ValueError(f"Translator {translator} returned a partial translation for call '{call_name}': {source_args} -> {destionation_args}")
                    names_index.setdefault(call_name.lower(), call_name)
                    argument_names_table.setdefault(call_name.lower(), res)
                    return
            raise ValueError(f"No translation found for API '{call_name}': {source_args} -> {destionation_args}")
                    
                    
        with alive_bar(len(source_format_dict) * 2, title = "Analyzing and translating API calls") as bar:
            for src_call, src_names in source_format_dict.items():
                if src_call in destination_format_dict:
                    translate_call(src_call, src_names, destination_format_dict[src_call])
                else:
                    translate_call(src_call, src_names, set())
                bar()
                src_call_lower = src_call.lower()
                for src_arg_name in src_names:
                    if src_arg_name in argument_names_table.get(src_call_lower, {}):
                        dst_arg_name = argument_names_table[src_call_lower][src_arg_name]
                        if src_arg_name in source_type_dict.get(src_call, {}) and dst_arg_name in destination_type_dict.get(src_call,{}):
                            src_arg_type_counts, dst_arg_type_counts = source_type_dict[src_call][src_arg_name].most_common(), destination_type_dict[src_call][dst_arg_name].most_common()
                            if len(src_arg_type_counts) == 1 and len(dst_arg_type_counts) == 1:
                                src_arg_type, dst_arg_type = src_arg_type_counts[0][0], dst_arg_type_counts[0][0]
                            match (src_arg_type, dst_arg_type):
                                case ("int", "str"):
                                    argument_types_table.setdefault(src_call_lower, {})[src_arg_name] = "int_to_str"
                                case ("int", "hex_string"):
                                    argument_types_table.setdefault(src_call_lower, {})[src_arg_name] = "int_to_hex"
                                case ("int_string", "int"):
                                    argument_types_table.setdefault(src_call_lower, {})[src_arg_name] = "str_to_int"
                                case ("hex_string", "int"):
                                    argument_types_table.setdefault(src_call_lower, {})[src_arg_name] = "hex_to_int"
                bar()
                                

        with output.open("w") as f:
            dump(translation_table, f, indent="\t")

        
    env = {
        "cuckoo_reader" : cuckoo_reader,
        "cape_reader" : cape_reader,
        "progress" : progress,
        "start_scanning_source_format" : start_scanning_source_format,
        "start_scanning_destination_format" : start_scanning_destination_format,
        "wait_scans" : wait_scans,
        "levenshtein_assignement_translate" : levenshtein_assignement_translate,
        "translate" : translate
    }

    code : str | None = args.code

    if code is not None:

        exec(code, {}, env)
    
    else:

        InteractiveInterpreter(env).interact("Interactive Automatic Translator Prompt. Use dir() to see the useful functions.")





if __name__ == "__main__":
    main()